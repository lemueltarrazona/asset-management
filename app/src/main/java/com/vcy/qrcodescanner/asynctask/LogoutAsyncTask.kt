package com.vcy.qrcodescanner.asynctask

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.dialog.LogoutDialog
import com.vcy.qrcodescanner.miscellaneous.URLLists
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class LogoutAsyncTask(var context:Context):AsyncTask<String,String,String>(){
    private var progressDialog=ProgressDialog(context)
    private var sharedPreferences=context.getSharedPreferences("login",Context.MODE_PRIVATE)
    private var token=sharedPreferences.getString("token","")
    private var email=sharedPreferences.getString("email","")
    private var code=0
    private var logoutDialog=LogoutDialog()

    override fun onPreExecute() {
        progressDialog.setMessage("Log Out")
        progressDialog.setCancelable(false)
        progressDialog.show()
        super.onPreExecute()
    }

    override fun doInBackground(vararg params: String?): String? {
        try{
            val url = URL(URLLists.logout_url)
            val conn:HttpURLConnection = url.openConnection() as HttpURLConnection
            conn.requestMethod="POST"
            conn.setRequestProperty("Authorization",token)
            val uriBuilder= Uri.Builder()
                    .appendQueryParameter("email",email)
            val query = uriBuilder.build().encodedQuery
            val outputStream = conn.outputStream
            val bufferedWriter = BufferedWriter(OutputStreamWriter(outputStream,"UTF-8"))
            bufferedWriter.write(query)
            bufferedWriter.close()
            outputStream.close()
            conn.connect()
            code=conn.responseCode
        }catch(e:IOException){
        }
        return null
    }

    override fun onPostExecute(result: String?) {
        progressDialog.dismiss()
        if(code!=200){
            val message = "Log out failed. \n${context.getString(R.string.conn_error)}"
            logoutDialog.error(context,message)
        } else{
            val edit:SharedPreferences.Editor=sharedPreferences.edit()
            edit.clear()
            edit.apply()
            val message="Log out Success"
            logoutDialog.logout(context,message)
        }
        super.onPostExecute(result)
    }
}
