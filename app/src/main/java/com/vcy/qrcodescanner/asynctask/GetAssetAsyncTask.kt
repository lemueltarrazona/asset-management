package com.vcy.qrcodescanner.asynctask

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.activity.AssetActivity
import com.vcy.qrcodescanner.dialog.AssetDialog
import com.vcy.qrcodescanner.miscellaneous.CheckNull
import com.vcy.qrcodescanner.miscellaneous.URLLists
import com.vcy.qrcodescanner.model.HistoryDetailsModel
import com.vcy.qrcodescanner.model.HistoryModel
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class GetAssetAsyncTask(var context: Context, var date: String, var time: String) : AsyncTask<String, Int, String>() {
    @SuppressLint("StaticFieldLeak")
    private var token = context.getSharedPreferences(context.getString(R.string.credentials), Context.MODE_PRIVATE)
    private var getToken = token.getString("token", "")
    private var check = CheckNull()
    private var getResponseCode: Int = 0
    private var string = String()
    private var assetCode = String()
    private var type = String()
    private var subtype = String()
    private var description = String()
    private var company = String()
    private var branch = String()
    private var department = String()
    private var user = String()
    private var serial = String()
    private var model = String()
    private var brand = String()
    private var sap = String()
    private var dPurchase = String()
    private var dIssue = String()
    private var status = String()
    private var retirement = String()
    private var additionalInfo = String()
    private var dialog = AssetDialog()
    private var historyDetails = HistoryDetailsModel(context)
    private var history = HistoryModel(context)
    private val progressDialog = ProgressDialog(context)

    override fun onPreExecute() {
        super.onPreExecute()
        progressDialog.setMessage("Sending...")
        progressDialog.setCancelable(false)
        progressDialog.show()
    }

    override fun doInBackground(vararg params: String): String {
        try {
            val url = URL(URLLists.data_url + params[0])
            val apiConnection = url.openConnection() as HttpURLConnection
            apiConnection.requestMethod = "GET"
            apiConnection.setRequestProperty("Authorization", getToken)
            apiConnection.setRequestProperty("Accept", "application/json")
            apiConnection.connect()
            getResponseCode = apiConnection.responseCode
            val inputStream = apiConnection.inputStream
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringResult = StringBuilder()
            var result: String? = null
            while ({ result = bufferedReader.readLine();result }() != null) {
                stringResult.append(result)
            }
            assetCode = params[0]
            string = stringResult.toString()
            val getJsonObject = JSONObject(string)
            try {
                type = check.check(getJsonObject.getString("assettype"))
                subtype = check.check(getJsonObject.getString("subtype"))
                description = check.check(getJsonObject.getString("description"))
                company = check.check(getJsonObject.getString("company"))
                branch = check.check(getJsonObject.getString("branch"))
                department = check.check(getJsonObject.getString("department"))
                user = check.check(getJsonObject.getString("person"))
                serial = check.check(getJsonObject.getString("serial"))
                model = check.check(getJsonObject.getString("model"))
                brand = check.check(getJsonObject.getString("brand"))
                sap = check.check(getJsonObject.getString("sap_ref_no"))
                dPurchase = check.check(getJsonObject.getString("purchased_date"))
                dIssue = check.check(getJsonObject.getString("issued_date"))
                status = check.check(getJsonObject.getString("status_desc"))
                retirement = check.check(getJsonObject.getString("retirement_date"))
                additionalInfo = check.check(getJsonObject.getString("addtl_info"))
            } catch (e: Exception) {
            }
        } catch (e: Exception) {
        } catch (e: MalformedURLException) {
        } catch (e: IOException) {
        } catch (e: JSONException) {
        }
        return ""
    }

    override fun onProgressUpdate(vararg values: Int?) {
        super.onProgressUpdate(*values)
        values[0]?.let { progressDialog.progress = it }
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        progressDialog.dismiss()

        if (getResponseCode == 200) {
            if (string != "{}") {
                AssetActivity.assetEt.text = assetCode
                AssetActivity.typeEt.text = type
                AssetActivity.subtypeEt.text = subtype
                AssetActivity.descriptionEt.text = description
                AssetActivity.companyEt.text = company
                AssetActivity.branchEt.text = branch
                AssetActivity.departmentEt.text = department
                AssetActivity.userEt.text = user
                AssetActivity.serialEt.text = serial
                AssetActivity.modelEt.text = model
                AssetActivity.brandEt.text = brand
                AssetActivity.sapEt.text = sap
                AssetActivity.purchaseEt.text = dPurchase
                AssetActivity.issueEt.text = dIssue
                AssetActivity.statusEt.text = status
                AssetActivity.retirementEt.text = retirement
                AssetActivity.additionalInfoEt.text = additionalInfo

                history.add(assetCode, date, time)
                historyDetails.add(assetCode, type, subtype, description, company, department, user, serial, model, brand, sap,
                        dPurchase, dIssue, status, retirement, branch, additionalInfo)
            } else {
                clear()
                val stringMessage = "Asset does not exist"
                dialog.error(context, stringMessage)
            }
        } else {
            clear()
            val stringMessage = "No connection.\n${context.getString(R.string.conn_error)}"
            dialog.error(context, stringMessage)
        }
    }

    private fun clear() {
        AssetActivity.assetEt.text = ""
        AssetActivity.typeEt.text = ""
        AssetActivity.subtypeEt.text = ""
        AssetActivity.descriptionEt.text = ""
        AssetActivity.companyEt.text = ""
        AssetActivity.branchEt.text = ""
        AssetActivity.departmentEt.text = ""
        AssetActivity.userEt.text = ""
        AssetActivity.serialEt.text = ""
        AssetActivity.modelEt.text = ""
        AssetActivity.brandEt.text = ""
        AssetActivity.sapEt.text = ""
        AssetActivity.purchaseEt.text = ""
        AssetActivity.issueEt.text = ""
        AssetActivity.statusEt.text = ""
        AssetActivity.retirementEt.text = ""
    }
}