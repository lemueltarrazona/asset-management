package com.vcy.qrcodescanner.asynctask

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import android.util.Log
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.miscellaneous.URLLists
import com.vcy.qrcodescanner.activity.HomeActivity
import com.vcy.qrcodescanner.dialog.LoginDialog
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.URL
import java.net.MalformedURLException as MalformedURLException1

class LoginAsyncTask(var context: Context) : AsyncTask<String, Int, String>() {
    private var getResponseCode: Int = 0
    private var getToken = String()
    private var email: String? = String()
    private var dialog = LoginDialog()
    @SuppressLint("StaticFieldLeak")
    private val sharedPreferences = context.getSharedPreferences(context.getString(R.string.credentials), Context.MODE_PRIVATE)
    private var sharedTokenEditor = sharedPreferences.edit()
    private val progressDialog = ProgressDialog(context)

    override fun onPreExecute() {
        super.onPreExecute()
        progressDialog.setMessage("Connecting")
        progressDialog.setCancelable(false)
        progressDialog.show()
    }

    override fun doInBackground(vararg params: String?): String? {
        try {
            val urlLogin = URL(URLLists.login_url)
            val apiConnection = urlLogin.openConnection() as HttpURLConnection
            apiConnection.requestMethod = "POST"
            apiConnection.setRequestProperty("Accept", "application/json")
            val parameters = Uri.Builder()
                    .appendQueryParameter("googleAuthCode", params[0])
            val query = parameters.build().encodedQuery
            val outputStream = apiConnection.outputStream
            val bufferedWriter = BufferedWriter(OutputStreamWriter(outputStream, "UTF-8"))
            bufferedWriter.write(query)
            bufferedWriter.close()
            outputStream.close()
            apiConnection.connect()
            getResponseCode = apiConnection.responseCode
            val inputStream = apiConnection.inputStream
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringResult = StringBuilder()
            var result: String? = null
            while ({ result = bufferedReader.readLine();result }() != null) {
                stringResult.append(result)
            }
            if (getResponseCode == 200) {
                val resultObject = JSONObject(stringResult.toString())
                email = resultObject.get("email") as String
                getToken = resultObject.get("token") as String
                val authToken = "Bearer $getToken"
                sharedTokenEditor.putString("token", authToken)
                sharedTokenEditor.apply()
            }
            apiConnection.disconnect()
        } catch (e: MalformedURLException1) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return null
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        progressDialog.dismiss()
        val activity = context as Activity
        when {
            getResponseCode == 200 -> {
                val edit: SharedPreferences.Editor = sharedPreferences.edit()
                edit.putString("email", email)
                edit.apply()
                val openMain = Intent(context, HomeActivity::class.java)
                context.startActivity(openMain)
                activity.finish()
            }

            getResponseCode == 401 && getToken == "" -> {
                val stringMessage = "Invalid Username or Password"
                dialog.error(context, stringMessage)
            }
            else -> {
                val stringMessage = "Error $getResponseCode: Login failed. \n${context.getString(R.string.conn_error)}"
                dialog.error(context, stringMessage)
                sharedTokenEditor = sharedPreferences.edit()
                sharedTokenEditor.clear()
                sharedTokenEditor.apply()
            }
        }
    }
}
