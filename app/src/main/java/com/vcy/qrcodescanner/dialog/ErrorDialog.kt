package com.vcy.qrcodescanner.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.vcy.qrcodescanner.R

class ErrorDialog {
    private lateinit var googleSignInOptions:GoogleSignInOptions
    private lateinit var googleSignInClient: GoogleSignInClient

    fun error(context:Context,error:String,message:String){
        val activity:Activity=context as Activity
        val builder=AlertDialog.Builder(context)
        builder.setMessage("Error $error: $message")
                .setPositiveButton("Confirm"){_,_->
                    run {
                        googleSignInOptions = GoogleSignInOptions.Builder()
                                .requestServerAuthCode(context.getString(R.string.google_auth_code))
                                .requestEmail()
                                .build()
                        googleSignInClient=GoogleSignIn.getClient(context,googleSignInOptions)
                        googleSignInClient.signOut().addOnCompleteListener(activity){
                            googleSignInClient.revokeAccess().addOnCompleteListener(activity){}
                        }
                    }
                }
        val dialog=builder.create()
        dialog.show()
    }
}