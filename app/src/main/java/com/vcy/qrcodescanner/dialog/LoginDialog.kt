package com.vcy.qrcodescanner.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.vcy.qrcodescanner.R

class LoginDialog{
    private val confirm="Confirm"
    fun error(context:Context,message:String){
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(confirm){_,_->
                    val activity:Activity = context as Activity
                    val googleSignInOptions:GoogleSignInOptions= GoogleSignInOptions.Builder()
                            .requestServerAuthCode(context.getString(R.string.google_auth_code))
                            .requestEmail()
                            .build()
                    val googleSignInClient:GoogleSignInClient= GoogleSignIn.getClient(context,googleSignInOptions)
                    googleSignInClient.signOut().addOnCompleteListener(activity){
                        googleSignInClient.revokeAccess().addOnCompleteListener(activity){}
                    }
                }
        val dialog = builder.create()
        dialog.show()
    }
}