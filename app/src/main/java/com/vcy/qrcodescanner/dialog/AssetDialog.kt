package com.vcy.qrcodescanner.dialog

import android.app.AlertDialog
import android.content.Context

class AssetDialog {
    var confirm:String = "Confirm"
    fun error(context:Context, stringMessage:String){
        val builderMessage = AlertDialog.Builder(context)
        builderMessage.setMessage(stringMessage)
                .setCancelable(true)
                .setPositiveButton(confirm) { _, _ ->
                }
        val dialog:AlertDialog = builderMessage.create()
        dialog.show()
    }
}