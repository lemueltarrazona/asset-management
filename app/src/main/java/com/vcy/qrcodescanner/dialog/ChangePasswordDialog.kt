package com.vcy.qrcodescanner.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import com.vcy.qrcodescanner.activity.LoginActivity

class ChangePasswordDialog {

    var confirm = "Confirm"
    var deny = "Cancel"

    fun success(context: Context, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(confirm) { _, _ ->
                    val sharedPreferences = context.getSharedPreferences("login",Context.MODE_PRIVATE)
                    val edit = sharedPreferences.edit()
                    edit.clear()
                    edit.apply()
                    val activity: Activity = context as Activity
                    val intent = Intent(context, LoginActivity::class.java)
                    context.startActivity(intent)
                    activity.finish()
                }
        val dialog = builder.create()
        dialog.show()
    }

    fun error(context: Context, stringMessage: String) {
        val builderMessage = AlertDialog.Builder(context)
        builderMessage.setMessage(stringMessage)
                .setCancelable(true)
                .setPositiveButton(confirm) { _, _ ->
                }
        val dialog: AlertDialog = builderMessage.create()
        dialog.show()
    }
}