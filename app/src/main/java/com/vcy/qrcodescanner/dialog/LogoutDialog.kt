package com.vcy.qrcodescanner.dialog

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.activity.GoogleLoginActivity
import com.vcy.qrcodescanner.asynctask.LogoutAsyncTask

class LogoutDialog {
    private val confirm = "Confirm"
    private val deny = "Cancel"
    private lateinit var googleSignInOptions:GoogleSignInOptions
    private lateinit var googleSignInClient: GoogleSignInClient

    fun confirmation(context: Context, message: String) {
        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(context.getString(R.string.google_auth_code))
                .requestEmail()
                .build()
        googleSignInClient = GoogleSignIn.getClient(context,googleSignInOptions)
        val builder = AlertDialog.Builder(context)
        val activity: Activity = context as Activity
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(confirm) { _, _ ->
                    googleSignInClient.signOut().addOnCompleteListener(activity) {
                        googleSignInClient.revokeAccess().addOnCompleteListener(activity) {
                            LogoutAsyncTask(context).execute()
                        }
                    }
                }
                .setNegativeButton(deny) { _, _ -> }
        val dialog = builder.create()
        dialog.show()
    }

    fun error(context: Context, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(confirm) { _, _ -> }
        val dialog = builder.create()
        dialog.show()
    }

    fun logout(context: Context, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(confirm) { _, _ ->
                    val activity: Activity = context as Activity
                    val intent = Intent(context, GoogleLoginActivity::class.java)
                    context.startActivity(intent)
                    activity.finish()
                }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
