package com.vcy.qrcodescanner.model

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.vcy.qrcodescanner.database.AssetManagementDatabase

import java.util.HashMap
import java.util.ArrayList


class HistoryModel(private var context:Context) {
    companion object{
        const val TBL_NAME = "tbl_history"
        const val ID = "id"
        const val ASSET_CODE = "asset_code"
        const val SCAN_DATE = "scan_date"
        const val TIME = "time"

        const val create = "CREATE TABLE $TBL_NAME ($ID INTEGER PRIMARY KEY AUTOINCREMENT, $ASSET_CODE VARCHAR, " +
                "$SCAN_DATE, VARCHAR, $TIME VARCHAR)"
        const val upgrade = "DROP TABLE IF EXISTS $TBL_NAME"
    }

    fun add(asset:String,scan_date:String,time:String){
        val dbWrite:SQLiteDatabase = AssetManagementDatabase.getInstance(context).writableDatabase
        val data = ContentValues()
        data.put(ASSET_CODE,asset)
        data.put(SCAN_DATE,scan_date)
        data.put(TIME,time)
        dbWrite.insert(TBL_NAME,null,data)
        dbWrite.close()
    }

    fun selectHeader(date:String): ArrayList<HashMap<String,String>>{
        val dbRead:SQLiteDatabase = AssetManagementDatabase.getInstance(context).readableDatabase
        val list=ArrayList<HashMap<String,String>>()
        val select = "SELECT * FROM $TBL_NAME WHERE $SCAN_DATE = '$date' ORDER BY $SCAN_DATE DESC"
        val query = dbRead.rawQuery(select,null)
        query.moveToFirst()
        while(!query.isAfterLast){
            val data = HashMap<String,String>()
            data[ID] = query.getString(query.getColumnIndex(ID))
            data[ASSET_CODE] = query.getString(query.getColumnIndex(ASSET_CODE))
            data[TIME] = query.getString(query.getColumnIndex(TIME))
            list.add(data)
            query.moveToNext()
        }
        dbRead.close()
        query.close()
        return list
    }

    fun searchHeader(date:String,assetCode:String): ArrayList<HashMap<String,String>>{
        val dbRead:SQLiteDatabase = AssetManagementDatabase.getInstance(context).readableDatabase
        val list=ArrayList<HashMap<String,String>>()
        val select = "SELECT * FROM $TBL_NAME WHERE $SCAN_DATE = '$date' AND $ASSET_CODE LIKE '$assetCode%' ORDER BY $SCAN_DATE DESC"
        val query = dbRead.rawQuery(select,null)
        query.moveToFirst()
        while(!query.isAfterLast){
            val data = HashMap<String,String>()
            data[ID] = query.getString(query.getColumnIndex(ID))
            data[ASSET_CODE] = query.getString(query.getColumnIndex(ASSET_CODE))
            data[TIME] = query.getString(query.getColumnIndex(TIME))
            list.add(data)
            query.moveToNext()
        }
        dbRead.close()
        query.close()
        return list
    }
}