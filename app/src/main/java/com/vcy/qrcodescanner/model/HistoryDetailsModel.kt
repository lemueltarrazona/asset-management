package com.vcy.qrcodescanner.model

import android.content.ContentValues
import android.content.Context
import android.util.Log
import com.vcy.qrcodescanner.database.AssetManagementDatabase

class HistoryDetailsModel(private val context: Context) {
    companion object {
        const val TBL_NAME = "tbl_history_details"
        const val ID = "id"
        const val ASSET_CODE = "asset_code"
        const val TYPE = "type"
        const val SUBTYPE = "subtype"
        const val DESCRIPTION = "description"
        const val COMPANY = "company"
        const val DEPARTMENT = "department"
        const val USER = "user"
        const val SERIAL = "serial"
        const val MODEL = "model"
        const val BRAND = "brand"
        const val SAP_REF_NO = "sap_ref_no"
        const val PURCHASE_DATE = "purchase_date"
        const val ISSUE_DATE = "issue_date"
        const val STATUS = "status"
        const val RETIREMENT_DATE = "retirement_date"
        //alter
        const val BRANCH = "branch"
        //alter
        const val ADDITIONAL_INFO = "additional_info"

        const val create = "CREATE TABLE $TBL_NAME ($ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "$ASSET_CODE VARCHAR, $TYPE VARCHAR, $SUBTYPE VARCHAR, $DESCRIPTION VARCHAR, " +
                "$COMPANY VARCHAR, $DEPARTMENT VARCHAR, $USER VARCHAR, $SERIAL VARCHAR, $MODEL VARCHAR, " +
                "$BRAND VARCHAR, $SAP_REF_NO VARCHAR, $PURCHASE_DATE VARCHAR, $ISSUE_DATE VARCHAR," +
                "$STATUS VARCHAR, $RETIREMENT_DATE VARCHAR, $ADDITIONAL_INFO VARCHAR, $BRANCH, VARCHAR)"

        const val drop = "DROP TABLE IF EXISTS $TBL_NAME"
    }

    fun add(asset_code: String,type:String,subtype:String, description: String, company: String, department: String, user: String,
            serial: String, model: String, brand: String, sap_ref_no: String, purchase_date: String,
            issue_date: String, status: String, retirement_date: String,branch:String,additionalInfo:String) {
        val dbWrite = AssetManagementDatabase.getInstance(context).writableDatabase
        val data = ContentValues()
        data.put(ASSET_CODE, asset_code)
        data.put(TYPE,type)
        data.put(SUBTYPE,subtype)
        data.put(DESCRIPTION, description)
        data.put(COMPANY, company)
        data.put(DEPARTMENT, department)
        data.put(USER, user)
        data.put(SERIAL, serial)
        data.put(MODEL, model)
        data.put(BRAND, brand)
        data.put(SAP_REF_NO, sap_ref_no)
        data.put(PURCHASE_DATE, purchase_date)
        data.put(ISSUE_DATE, issue_date)
        data.put(STATUS, status)
        data.put(RETIREMENT_DATE, retirement_date)
        data.put(BRANCH,branch)
        data.put(ADDITIONAL_INFO,additionalInfo)
        dbWrite.insert(TBL_NAME, null, data)
    }

    fun selectAll():ArrayList<HashMap<String,String>>{
        val db=AssetManagementDatabase.getInstance(context).readableDatabase
        val result=ArrayList<HashMap<String,String>>()
        val select = "SELECT * FROM $TBL_NAME"
        val query=db.rawQuery(select,null)
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    val data = HashMap<String, String>()
                    data[ASSET_CODE] = query.getString(query.getColumnIndex(ASSET_CODE))
                    data[TYPE] = query.getString(query.getColumnIndex(TYPE))
                    data[SUBTYPE] = query.getString(query.getColumnIndex(SUBTYPE))
                    data[DESCRIPTION] = query.getString(query.getColumnIndex(DESCRIPTION))
                    data[COMPANY] = query.getString(query.getColumnIndex(COMPANY))
                    data[DEPARTMENT] = query.getString(query.getColumnIndex(DEPARTMENT))
                    data[USER] = query.getString(query.getColumnIndex(USER))
                    data[SERIAL] = query.getString(query.getColumnIndex(SERIAL))
                    data[MODEL] = query.getString(query.getColumnIndex(MODEL))
                    data[BRAND] = query.getString(query.getColumnIndex(BRAND))
                    data[SAP_REF_NO] = query.getString(query.getColumnIndex(SAP_REF_NO))
                    data[PURCHASE_DATE] = query.getString(query.getColumnIndex(PURCHASE_DATE))
                    data[ISSUE_DATE] = query.getString(query.getColumnIndex(ISSUE_DATE))
                    data[STATUS] = query.getString(query.getColumnIndex(STATUS))
                    data[RETIREMENT_DATE] = query.getString(query.getColumnIndex(RETIREMENT_DATE))
                    data[BRANCH] = query.getString(query.getColumnIndex(BRANCH))
                    data[ADDITIONAL_INFO] = query.getString(query.getColumnIndex(ADDITIONAL_INFO))
                    result.add(data)
                } while (query.moveToNext())
            }
        }
        query.close()
        db.close()
        Log.e("result","$result")
        return result
    }

    fun select(asset_code: String, id: String): ArrayList<HashMap<String, String>> {
        val dbRead = AssetManagementDatabase.getInstance(context).readableDatabase
        val result = ArrayList<HashMap<String, String>>()
        val select = "SELECT * FROM $TBL_NAME WHERE $ASSET_CODE = '$asset_code' AND $ID = '$id'"
        val query = dbRead.rawQuery(select, null)
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    val data = HashMap<String, String>()
                    data[ASSET_CODE] = query.getString(query.getColumnIndex(ASSET_CODE))
                    data[TYPE] = query.getString(query.getColumnIndex(TYPE))
                    data[SUBTYPE] = query.getString(query.getColumnIndex(SUBTYPE))
                    data[DESCRIPTION] = query.getString(query.getColumnIndex(DESCRIPTION))
                    data[COMPANY] = query.getString(query.getColumnIndex(COMPANY))
                    data[DEPARTMENT] = query.getString(query.getColumnIndex(DEPARTMENT))
                    data[USER] = query.getString(query.getColumnIndex(USER))
                    data[SERIAL] = query.getString(query.getColumnIndex(SERIAL))
                    data[MODEL] = query.getString(query.getColumnIndex(MODEL))
                    data[BRAND] = query.getString(query.getColumnIndex(BRAND))
                    data[SAP_REF_NO] = query.getString(query.getColumnIndex(SAP_REF_NO))
                    data[PURCHASE_DATE] = query.getString(query.getColumnIndex(PURCHASE_DATE))
                    data[ISSUE_DATE] = query.getString(query.getColumnIndex(ISSUE_DATE))
                    data[STATUS] = query.getString(query.getColumnIndex(STATUS))
                    data[RETIREMENT_DATE] = query.getString(query.getColumnIndex(RETIREMENT_DATE))
                    data[BRANCH] = query.getString(query.getColumnIndex(BRANCH))
                    data[ADDITIONAL_INFO] = query.getString(query.getColumnIndex(ADDITIONAL_INFO))
                    result.add(data)
                } while (query.moveToNext())
            }
        }
        query.close()
        dbRead.close()
        Log.e("result","$result")
        return result
    }
}