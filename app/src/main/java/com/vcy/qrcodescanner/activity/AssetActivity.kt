package com.vcy.qrcodescanner.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.asynctask.GetAssetAsyncTask
import com.vcy.qrcodescanner.barcode.BarcodeCaptureActivity
import com.vcy.qrcodescanner.model.HistoryDetailsModel
import com.vcy.qrcodescanner.model.HistoryModel
import java.text.SimpleDateFormat
import java.util.*

class AssetActivity : AppCompatActivity() {
    companion object {
        private const val BARCODE_READER_REQUEST_CODE = 1

        @SuppressLint("StaticFieldLeak")
        lateinit var assetEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var typeEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var subtypeEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var descriptionEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var companyEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var branchEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var departmentEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var userEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var serialEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var modelEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var brandEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var sapEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var purchaseEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var issueEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var statusEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var retirementEt: TextView
        @SuppressLint("StaticFieldLeak")
        lateinit var additionalInfoEt: TextView
    }

    private lateinit var value: SharedPreferences
    private lateinit var historyDetails: HistoryDetailsModel
    private lateinit var history: HistoryModel
    private var stringAsset = String()
    private var date = Date()
    private var time = Date()
    private var stringDate = String()
    private var stringTime = String()
    @SuppressLint("SimpleDateFormat")
    private var timeFormat = SimpleDateFormat("hh:mm a")
    @SuppressLint("SimpleDateFormat")
    private var dateFormat = SimpleDateFormat("yyyy-MM-dd")


    private val context: Context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initials()
        value = getSharedPreferences("values", Context.MODE_PRIVATE)
        findViewById<Button>(R.id.scan_barcode_button).setOnClickListener {
            val intent = Intent(applicationContext, BarcodeCaptureActivity::class.java)
            startActivityForResult(intent, BARCODE_READER_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    val barcode = data.getParcelableExtra<Barcode>(BarcodeCaptureActivity.BarcodeObject)
                    val p = barcode.cornerPoints
                    assetEt.text = barcode.displayValue
                    stringAsset = assetEt.text.toString()
                    date = Calendar.getInstance().time
                    time = Calendar.getInstance().time
                    stringDate = dateFormat.format(date)
                    stringTime = timeFormat.format(time)

                    GetAssetAsyncTask(context, stringDate, stringTime).execute(stringAsset)

                    value = getSharedPreferences("values", Context.MODE_PRIVATE)

                } else
                    assetEt.text = ""
            } else
                assetEt.text = ""
        } else
            super.onActivityResult(requestCode, resultCode, data)
    }

    private fun initials(){
        setContentView(R.layout.activity_asset)
        assetEt = findViewById(R.id.asset_code)
        typeEt = findViewById(R.id.type)
        subtypeEt = findViewById(R.id.subtype)
        descriptionEt = findViewById(R.id.description)
        companyEt = findViewById(R.id.company)
        branchEt = findViewById(R.id.branch)
        departmentEt = findViewById(R.id.department)
        userEt = findViewById(R.id.user)
        serialEt = findViewById(R.id.serial)
        modelEt = findViewById(R.id.model)
        brandEt = findViewById(R.id.brand)
        sapEt = findViewById(R.id.sap)
        purchaseEt = findViewById(R.id.purchase_date)
        issueEt = findViewById(R.id.issued_date)
        statusEt = findViewById(R.id.status)
        retirementEt = findViewById(R.id.retirement_date)
        additionalInfoEt = findViewById(R.id.additional_info)
        history = HistoryModel(context)
        historyDetails = HistoryDetailsModel(context)
    }
}
