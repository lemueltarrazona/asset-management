package com.vcy.qrcodescanner.activity

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.SignInButton
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.asynctask.LoginAsyncTask

class LoginActivity : AppCompatActivity(){
    private lateinit var user : EditText
    private lateinit var password : EditText
    private lateinit var rememberCbx: CheckBox
    private lateinit var login:Button
    private lateinit var userString:String
    private lateinit var passwordString:String
    private lateinit var loginPreferences :SharedPreferences
    private lateinit var loginPreferencesEdit:SharedPreferences.Editor
    private var saved:Boolean=false
    private lateinit var context : Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initials()
        if(saved){
            user.setText(loginPreferences.getString("user",""))
            password.setText(loginPreferences.getString("password",""))

            rememberCbx.isChecked=true
            loginClick()
        }
        login.setOnClickListener { v-> loginClick() }
    }

    private fun loginClick() {
        userString=user.text.toString()
        passwordString=password.text.toString()
        if(checkNull()){
            loginPreferencesEdit = loginPreferences.edit()
            if(rememberCbx.isChecked){
                loginPreferencesEdit.putBoolean("saved",true)
                loginPreferencesEdit.putString("user",userString)
                loginPreferencesEdit.putString("password",passwordString)
                loginPreferencesEdit.apply()
            }
            else{
                loginPreferencesEdit.clear()
                loginPreferencesEdit.commit()
            }
            LoginAsyncTask(context).execute(userString,passwordString)
        }
    }

    private fun checkNull():Boolean{
        if(userString == ""){
            user.error = "Username is empty"
            return false
        }
        if(passwordString == ""){
            password.error = "Password is empty"
            return false
        }
        return true
    }

    private fun initials(){
        setContentView(R.layout.activity_login)
        context = this

        user = findViewById(R.id.userEt)
        password = findViewById(R.id.password_et)
        rememberCbx = findViewById(R.id.remember_cbx)
        login = findViewById(R.id.login_btn)
        loginPreferences = getSharedPreferences("login",Context.MODE_PRIVATE)
        saved = loginPreferences.getBoolean("saved",false)

    }
}