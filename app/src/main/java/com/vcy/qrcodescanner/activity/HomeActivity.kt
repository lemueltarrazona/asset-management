package com.vcy.qrcodescanner.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.dialog.LogoutDialog

class HomeActivity:AppCompatActivity(){
    private lateinit var assetBtn:Button
    private lateinit var logoutBtn:Button
    private lateinit var historyBtn:Button
    private lateinit var logoutDialog:LogoutDialog
    private lateinit var context:Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initials()
        assetBtn.setOnClickListener { view ->
            val intent = Intent(context,AssetActivity::class.java)
            context.startActivity(intent)
        }
        historyBtn.setOnClickListener { view->
            val intent = Intent(context,HistoryActivity::class.java)
            context.startActivity(intent)
        }
        logoutBtn.setOnClickListener { view->
            val message="Are you sure you want to Log out?"
            logoutDialog.confirmation(context,message)
        }
    }

    private fun initials(){
        setContentView(R.layout.activity_home)
        assetBtn=findViewById(R.id.assetBtn)
        historyBtn=findViewById(R.id.historyBtn)
        logoutBtn=findViewById(R.id.logoutBtn)
        context=this
        logoutDialog = LogoutDialog()
    }
}