package com.vcy.qrcodescanner.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.asynctask.LoginAsyncTask
import com.vcy.qrcodescanner.dialog.ErrorDialog

class GoogleLoginActivity : AppCompatActivity(){

    private val rcSignIn:Int =1
    private lateinit var googleSignInClients:GoogleSignInClient
    private lateinit var googleSignInOptions: GoogleSignInOptions
    private lateinit var context: Context
    private lateinit var error: ErrorDialog
    private lateinit var signInButton:SignInButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initials()
        signInButton.setOnClickListener{signIn()}
    }

    private fun signIn(){
        val signInIntent:Intent = googleSignInClients.signInIntent
        startActivityForResult(signInIntent,rcSignIn)
    }

    private fun configureGoogleSignIn(){
        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(context.getString(R.string.google_auth_code))
                .requestEmail()
                .build()
        googleSignInClients = GoogleSignIn.getClient(this,googleSignInOptions)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==rcSignIn){
            val task:Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInAccount(task)
        }
    }

    private fun handleSignInAccount(task:Task<GoogleSignInAccount>){
        try{
            val account = task.getResult(ApiException::class.java)
            login(account)
        }catch(e:ApiException){
            val statusCode=e.statusCode.toString()
            error.error(context,statusCode,context.getString(R.string.conn_error))
        }
    }

    private fun login(account:GoogleSignInAccount?){
        val serverAuthCode = account?.serverAuthCode
        LoginAsyncTask(context).execute(serverAuthCode)
    }

    private fun initials(){
        setContentView(R.layout.activity_google_login)
        signInButton = findViewById(R.id.sign_in_button)
        context = this
        configureGoogleSignIn()
        error= ErrorDialog()
    }
}