package com.vcy.qrcodescanner.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.ListView
import android.widget.SimpleAdapter
import androidx.appcompat.app.AppCompatActivity
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.miscellaneous.SearchAssetHistory
import com.vcy.qrcodescanner.model.HistoryDetailsModel
import com.vcy.qrcodescanner.model.HistoryModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class HistoryActivity : AppCompatActivity() {
    private lateinit var context: Context
    private lateinit var listAdapter: SimpleAdapter
    private lateinit var header: HistoryModel
    private lateinit var search: SearchAssetHistory
    private var date = Date()
    private var dateString = String()
    @SuppressLint("SimpleDateFormat")
    private var dateFormat = SimpleDateFormat("yyyy-MM-dd")

    companion object {
        lateinit var list: ArrayList<HashMap<String, String>>
        lateinit var historyList: ListView
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initials()
        list = header.selectHeader(dateString)
        listAdapter = SimpleAdapter(context, list, R.layout.list_history, arrayOf("asset_code", "id",
                "time"), intArrayOf(R.id.description_tv, R.id.idTv, R.id.time_tv))
        historyList.adapter = listAdapter
        historyList.setOnItemClickListener { adapterView, view, position, id ->
            val details = adapterView.adapter.getItem(position) as HashMap<String, String>
            val detailsId = details["id"]
            val assetCode = details["asset_code"]

            val openDetails = Intent(context, HistoryDetailActivity::class.java)
            openDetails.putExtra("id", detailsId)
            openDetails.putExtra("asset_code", assetCode)
            context.startActivity(openDetails)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        search.filter(context, menuInflater, menu, dateString)
        return super.onCreateOptionsMenu(menu)
    }

    private fun initials() {
        setContentView(R.layout.activity_history)
        historyList = findViewById(R.id.history_list)
        dateString = dateFormat.format(date)
        context = this
        list = ArrayList()
        header = HistoryModel(context)
        search = SearchAssetHistory()
    }
}