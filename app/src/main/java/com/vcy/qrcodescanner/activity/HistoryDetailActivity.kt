package com.vcy.qrcodescanner.activity

import android.content.Context
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.model.HistoryDetailsModel

class HistoryDetailActivity : AppCompatActivity() {
    private lateinit var assetCode: EditText
    private lateinit var type: EditText
    private lateinit var subtype: EditText
    private lateinit var description: EditText
    private lateinit var company: EditText
    private lateinit var branch:EditText
    private lateinit var department: EditText
    private lateinit var user: EditText
    private lateinit var serial: EditText
    private lateinit var model: EditText
    private lateinit var brand: EditText
    private lateinit var sap: EditText
    private lateinit var purchaseDate: EditText
    private lateinit var issuedDate: EditText
    private lateinit var status: EditText
    private lateinit var retirementDate: EditText
    private lateinit var additionalInfo: EditText

    private lateinit var historyDetails: HistoryDetailsModel
    private lateinit var list: ArrayList<HashMap<String, String>>

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initials()
        val intent = intent
        val assetCodeString: String = intent.getStringExtra("asset_code") as String
        val idString: String = intent.getStringExtra("id") as String

        list = historyDetails.select(assetCodeString, idString)
        assetCode.setText(list[0]["asset_code"])
        type.setText(list[0]["type"])
        subtype.setText(list[0]["subtype"])
        description.setText(list[0]["description"])
        company.setText(list[0]["company"])
        branch.setText(list[0]["branch"])
        user.setText(list[0]["user"])
        department.setText(list[0]["department"])
        serial.setText(list[0]["serial"])
        model.setText(list[0]["model"])
        brand.setText(list[0]["brand"])
        sap.setText(list[0]["sap_ref_no"])
        purchaseDate.setText(list[0]["purchase_date"])
        issuedDate.setText(list[0]["issue_date"])
        status.setText(list[0]["status"])
        retirementDate.setText(list[0]["retirement_date"])
        additionalInfo.setText(list[0]["additional_info"])
    }

    private fun initials() {
        setContentView(R.layout.activity_history_details)
        assetCode = findViewById(R.id.asset_code)
        type = findViewById(R.id.type)
        subtype = findViewById(R.id.subtype)
        description = findViewById(R.id.description)
        company = findViewById(R.id.company)
        branch = findViewById(R.id.branch)
        department = findViewById(R.id.department)
        user = findViewById(R.id.user)
        serial = findViewById(R.id.serial)
        model = findViewById(R.id.model)
        brand = findViewById(R.id.brand)
        sap = findViewById(R.id.sap)
        purchaseDate = findViewById(R.id.purchase_date)
        issuedDate = findViewById(R.id.issued_date)
        status = findViewById(R.id.status)
        retirementDate = findViewById(R.id.retirement_date)
        additionalInfo = findViewById(R.id.additional_info)

        context = this

        historyDetails = HistoryDetailsModel(context)
        list = ArrayList()
    }
}