package com.vcy.qrcodescanner.miscellaneous

import android.content.Context
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.SearchView
import android.widget.SimpleAdapter
import com.vcy.qrcodescanner.R
import com.vcy.qrcodescanner.activity.HistoryActivity
import com.vcy.qrcodescanner.model.HistoryModel

class SearchAssetHistory {
    fun filter(context: Context, menuInflater: MenuInflater, menu: Menu,date:String) {
        menuInflater.inflate(R.menu.dashboard, menu)
        val searchItem: MenuItem = menu.findItem(R.id.search)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(search: String): Boolean {
                HistoryActivity.list.clear()
                val searchReplace:String = search.replace("\'","\'\'")
                val history = HistoryModel(context)
                val list = history.searchHeader(date,searchReplace)
                val listAdapter = SimpleAdapter(context, list, R.layout.list_history, arrayOf("asset_code", "id",
                "time"), intArrayOf(R.id.description_tv, R.id.idTv, R.id.time_tv))
                HistoryActivity.historyList.adapter=listAdapter
                return true
            }
        })
    }
}