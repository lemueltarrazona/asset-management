package com.vcy.qrcodescanner.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.vcy.qrcodescanner.model.HistoryDetailsModel
import com.vcy.qrcodescanner.model.HistoryModel

class AssetManagementDatabase(context:Context):SQLiteOpenHelper(context, DB_NAME,null, DB_VERSION){

    companion object{
        private const val DB_VERSION = 11
        private const val DB_NAME = "db_asset_management"

        private var instance: AssetManagementDatabase? = null

        fun getInstance(context: Context): AssetManagementDatabase {
            if (instance == null) {
                instance = AssetManagementDatabase(context.applicationContext)
            }
            return instance as AssetManagementDatabase
        }
    }


    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL(HistoryModel.create)
        db.execSQL(HistoryDetailsModel.create)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        if(oldVersion<11){
            db!!.execSQL(HistoryDetailsModel.drop)
            db.execSQL(HistoryDetailsModel.create)
        }
    }
}
